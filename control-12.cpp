#include <iostream>
#include <string>

//string a int: int "variable" = atoi("variableString".c_str());

using namespace std;

class Calendario {
  int dia;
  int mes;
  int ano;
public:
  Calendario (int, int, int);
  void set_dia(int);
  void set_mes(int);
  void set_ano(int);
  int get_dia();
  int get_mes();
  int get_ano();
  void corregirFecha();
  string modelarFechaAb();
  string modelarFechaExt();
  void incremento(int);

};

Calendario::Calendario(int dia, int mes, int ano){
  this->dia = dia;
  this->mes = mes;
  this->ano = ano;
}
void Calendario::set_dia(int dia){
  this->dia = dia;
}
void Calendario::set_mes(int mes){
  this->mes = mes;
}
void Calendario::set_ano(int ano){
  this->ano = ano;
}
int Calendario::get_dia(){
  return this->dia;
}
int Calendario::get_mes(){
  return this->mes;
}
int Calendario::get_ano(){
  return this->ano;
}
string Calendario::modelarFechaAb(){
  string fechaAb="";
  fechaAb = to_string(this->dia)+"/"+to_string(this->mes)+"/"+to_string(this->ano);
  return fechaAb;
}
string Calendario::modelarFechaExt(){
  string fechaExt="", mesP;
  if(this->mes == 1){
    mesP="enero ";
  }else if(this->mes == 2){
    mesP="febrero ";
  }else if(this->mes == 3){
    mesP = "marzo ";
  }else if(this->mes == 4){
    mesP = "abril ";
  }else if(this->mes == 5){
    mesP = "mayo ";
  }else if(this->mes == 6){
    mesP = "junio ";
  }else if(this->mes == 7){
    mesP = "julio ";
  }else if(this->mes == 8){
    mesP = "agosto ";
  }else if(this->mes == 9){
    mesP = "setiembre ";
  }else if(this->mes == 10){
    mesP = "octubre ";
  }else if(this->mes == 11){
    mesP = "noviembre ";
  }else if(this->mes == 12){
    mesP = "diciembre ";
  }
  fechaExt = to_string(this->dia)+" de "+mesP+"de "+to_string(this->ano);

  return fechaExt;
}
void Calendario::corregirFecha(){
  do{
  if (this->mes == 1 || this->mes == 3 || this->mes==5 || this->mes==7 || this->mes==8 || this->mes==10 || this->mes==12){
    if(this->dia>31){
      this->mes++;
      this->dia-=31;
    }
  }else if(this->mes==4 || this->mes==6 || this->mes==9 || this->mes==11){
    this->mes++;
    this->dia-=30;
  }else if(this->mes==2){
    if((this->ano%4==0 && this->ano%100 != 0) || this->ano%400==0){
      if (this->dia>29){
        this->mes++;
        this->dia-=29;
      }
    }else{
      if (this->dia>28){
        this->mes++;
        this->dia-=28;
      }
    }
  }
}while (this->dia>31);
do{
  if (this->mes>12){
    this->ano++;
    this->mes-=12;
  }
}while (this->mes>12);
}
void Calendario::incremento(int dias){
  this->dia+=dias;
  corregirFecha();
}

int main (){
  int dia;
  int mes;
  int ano;
  cin >> dia;
  cin >> mes;
  cin >> ano;
  Calendario* fecha1 = new Calendario(dia, mes, ano);
  cout << fecha1->modelarFechaExt() << endl;
  fecha1->incremento(31);
  cout << fecha1->modelarFechaAb() << endl;
  return 0;
}
