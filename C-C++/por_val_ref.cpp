#include <iostream>
#include <string>

using std::cout;
using std::endl;

class Punto
{
public: 
	int posx, posy;
	Punto (int x, int y){
	this->posx = x;
	this->posy = y;
	}
	void pintar(){
		cout << "(" 
			<< this->posx << ","
			<< this->posy << ")" << endl;
	}
};

void funcion(Punto* punto)
{
	punto->posx++;
	punto->posy++;
}

int main()
{
	Punto* punto = new Punto (10,10);
	funcion (punto);
	punto->pintar();
	
	return 0;
}
