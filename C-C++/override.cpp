#include <iostream>
#include <string>

using namespace std;

class Animal
{
public:
	void comer();
};

class Perro : public Animal{
public:
	void comer();
	void comer(string);
};

class Persona
{
private:
	Perro* mascota;
public:
	void set_mascota(Perro* perrito)
	{
		this->mascota= perrito;
	}
	void alimentar()
	{
		this->mascota->comer();
	}
};

void Animal::comer()
{
	cout << "Animal comiendo..." << endl;
}

void Perro::comer()
{
	cout << "Perro comiendo..." << endl;
}

void Perro::comer(string comida)
{
	cout << "Perro comiendo..." << comida << endl;
}

int main ()
{
	Perro perro = Perro();
	perro.comer();
	perro.comer("carne");
	Perro* perro2 = new Perro();
	
	Persona* pepito = new Persona();
	pepito->set_mascota(perro2);
	pepito->alimentar();
	
	pepito->set_mascota(&perro);
	
	return 0;
}
