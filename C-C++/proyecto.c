//20151803 Ximena Marianne Cuzcano Chavez
//20152102 Andres Eduardo Moncada Vargas

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
struct FileTableNode mksmth(struct FileTableNode *, char[255], int );
void ls(struct FileTableNode *, char[255]);
struct FileTableNode * asignarPunteros(struct FileTableNode *, char[255]);
struct FileTableNode * posPadre(struct FileTableNode *, char[255]);

struct FileTableNode{
	char *nombre;
	int num_hijos;
	int es_dir;
	struct FileTableNode *padre;
	struct FileTableNode *primer_hijo;
	struct FileTableNode *hermano;
}

int main(){
	int rpta=1;
	int i;
	int j=0;
	char funcion[6];
	char argumentos[255];
	struct FileTableNode *raiz;
	struct FileTableNode nodo[100];
	raiz = &nodo[0];


	do{
		struct FileTableNode smth;
		printf("\n? ");
		//El usuario ingresa informacion por consola
		scanf("%s%s", &funcion, &argumentos);

		//Se llama a la funcion
		if(strcmp(funcion, "ls") == 0){
			ls(raiz, argumentos);
		}else if(strcmp(funcion, "mkdir") == 0){
			smth = mksmth(raiz, argumentos, 1);

			if(posPadre(raiz, argumentos) != NULL){
				printf("Carpeta %s creada exitosamente", smth.nombre);
				smth.padre = posPadre(raiz, argumentos);
				nodo[j] = smth;
				j++;
			}

		}else if(strcmp(funcion, "mkfile") == 0){
			smth = mksmth(raiz, argumentos, 0);

			if(posPadre(raiz, argumentos) != NULL){
				printf("Archivo %s creado exitosamente", smth.nombre);
				nodo[j] = smth;
				j++;
			}

		}else{
			printf("Error, el comando no existe");
		}

		//Se pregunta si desea continuar o no
		printf("\n? Desea continuar [1/0]? ");
		scanf("%d", &rpta);
	}while(rpta == 1);

	return 0;
}

struct FileTableNode mksmth(struct FileTableNode *raiz, char argumentos[255], int dir){	//make something para evitar duplicidad de m�todos
	struct FileTableNode carpeta;
	const char ch = '>';
	char *nombreSmth;
	char *pos;

	pos = strrchr(argumentos, ch);
	pos++;
	carpeta.nombre = pos;
	carpeta.num_hijos = 0;
	carpeta.es_dir=dir;

	carpeta.padre = posPadre(raiz, argumentos);

	//Si recien se esta creando la carpeta o archivo, primer_hijo y hermano son nulos

	return carpeta;
}

void ls(struct FileTableNode *raiz, char argumentos[255]){
	struct FileTableNode *ptr;

	//Obtener el puntero del primer elemento de la carpeta
	ptr = posPadre(raiz, argumentos);
	ptr = ptr->primer_hijo;

	//Imprime todos los elementos
	if( ptr != NULL ){
		while(ptr != NULL){
			printf("\n- %s", *ptr->nombre);
			ptr = ptr->hermano;
		}
	}else{
		printf("No existen hijos de este nodo");
	}
}

struct FileTableNode * asignarPunteros(struct FileTableNode *raiz, char ruta[255]){
	struct FileTableNode *ptr;
	ptr = raiz;

	//Seguir la ruta especificada nivel por nivel considerando el token ">"
	//Ir hasta el nodo padre
	//Ir hasta el ultimo de los hermanos
	return ptr;
}

struct *FileTableNode posPadre(struct FileTableNode *raiz, char ruta[255]){
	struct FileTableNode *ptr;
	ptr = raiz;
	int a = 1;
	int i = 1;
	int j = 0;
	int k;
	int m = 0;
	char nombre[16];
	char nombres[16][16]; //Soportara un maximo de 15 carpetas anidadas y se le suma 1 porque la primera fila esta vacia

	//Copiar los nombres del path para llegar a la carpeta o archivo a crear
	//Notar que comienza en la fila 1, no en la cero
	for(k = 0; k<255; k++){
		if(ruta[k] == '>'){
			j++;
			m=0;
		}else{
			nombres[m][j]=ruta[k];
			m++;
		}
	}

	//Se resta 1 porque se considera la carpeta o archivo creandose
	j--;
	printf("%d carpetas por recorrer", j);


	//j es la cantidad de subcarpetas a recorrer hasta llegar al nodo padre
	if(j != 0){
		//Ir hasta el nodo padre
		do{
			//Comparar nivel por nivel
			//En caso de encontrar LA CARPETA del path, incrementar i en 1

			//Obtener el nombre de la carpeta
			for(k=0; k<16; k++){
				nombre[k] = nombres[k][i];
			}

			//printf("\n%d Hasta aca esta bien, carpeta %c.", i, nombre[0]);
			if(strcmp(nombre, ptr->nombre) == 0){
				//Como es igual, se 'entra' en la CARPETA
				ptr = ptr->primer_hijo;
				i++;	//Al legar a esta expresion se han matcheado 'i' carpetas de un total de 'j' carpetas
			}else{
				//Se busca en el hermano

				if((ptr) != NULL){
					ptr = ptr->hermano;
				}else{
					printf("No existe la ruta esecificada");
					a = 0;
					break;
				}
			}
		}while(i <= j);
	}


	if(a == 1){
		return ptr;
	}else{
		//
		return NULL;
	}

}
