#include <iostream>
#include <string>

using std::cout;
using std::endl;

class Rectangulo
{
public:
	int alto, ancho;
	int calcular_area(){
		return this->alto * this->ancho;
	}
};

class Cuadrado : public Rectangulo
{
public:
	int calcular_area(){
		return this->alto * this->alto;
	}
};

void pintar(Rectangulo* rect){
	cout << rect->calcular_area() << endl;
}

int main(){
	Cuadrado* cuad = new Cuadrado();
	cuad->alto=40;
	cuad->calcular_area();
	
	pintar(cuad);
	
	return 0;
}
