#include <iostream>
class Alumno
{
  char* codigo;
  char* nombre;
  float nota;
  Alumno* sig_alumno;
  public:
  Alumno (char*, char*, float );
  void set_codigo(char*);
  char* get_codigo();
  void set_nombre(char*);
  char* get_nombre();
  void set_nota(float);
  int get_nota();
  void agregar_sig_alumno(Alumno*);
  Alumno* obtener_sig_alumno();

};
Alumno::Alumno(char* codigo, char* nombre, float nota)
{
  this->codigo = codigo;
  this->nombre = nombre;
  this->nota=nota;
}
void Alumno::set_codigo(char* codigo)
{
  this->codigo=codigo;
}
char* Alumno::get_codigo()
{
  return this->codigo;
}
void Alumno::set_nombre(char* nombre)
{
  this->nombre=nombre;
}
char* Alumno::get_nombre()
{
  return this->nombre;
}
void Alumno::set_nota(float nota)
{
  this->nota=nota;
}
int Alumno::get_nota()
{
  return this->nota;
}
void Alumno::agregar_sig_alumno(Alumno* sig_alumno)
{
  this->sig_alumno = sig_alumno;
}
Alumno* Alumno::obtener_sig_alumno()
 {
   return this->sig_alumno;
 }
 float suma=0.0f;
 int tam=0;
 float prom=0;
 float promedio(Alumno* primer_alumno){
   if (primer_alumno != NULL) {
     suma=suma+primer_alumno->get_nota();
     tam++;
     return promedio(primer_alumno->obtener_sig_alumno());
   }else{
     prom=suma/tam;
     return prom;
   }

 }

int main(){
  char nombre[200]="Pepito";
  char codigo[9]="20164545";
  float nota_pepito=16.0f;
  Alumno* alumno = new Alumno(codigo, nombre, nota_pepito);
  // std::cout << alumno->get_codigo() << std::endl;
  // std::cout << alumno->get_nombre() << std::endl;
  // std::cout << alumno->get_nota() << std::endl;
  char cod[9]="20163782";
  char nom[200]="Juan";
  Alumno* alumno1 = new Alumno(cod, nom, 14.0f);
  alumno->agregar_sig_alumno(alumno1);
  // std::cout << alumno1->get_codigo() << std::endl;
  // std::cout << alumno1->get_nombre() << std::endl;
  // std::cout << alumno1->get_nota() << std::endl;
  char cod1[9]="20160708";
  char nom1[200]="Marcos";
  char cod2[9]="20114507";
  char cod3[9]="20140980";
  char nom2[200]="Andres";
  char nom3[200]="Jean";
  Alumno* alumno2 = new Alumno(cod1, nom1, 20.0f);
  alumno1->agregar_sig_alumno(alumno2);
  Alumno* alumno3 = new Alumno(cod2, nom2, 13.0f);
  alumno2->agregar_sig_alumno(alumno3);
  Alumno* alumno4 = new Alumno(cod3, nom3, 18.0f);
  alumno3->agregar_sig_alumno(alumno4);
  alumno4->agregar_sig_alumno(NULL);
  std::cout << promedio(alumno) << std::endl;
  return 0;
}
