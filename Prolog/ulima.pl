/*
Esto es un comentario
*/

alumno(gianvictor,ulima).
alumno(ximena,ulima).
companeros(Persona1,Persona2):-
  alumno(Persona1,ulima),
  alumno(Persona2,ulima).

padre(carlos, juan).
padre(carlos, carla).
padre(juan, pablo).
padre(juan, mateo).
padre(pedro, andres).
padre(andres, edwin).
madre(maria, juan).
madre(maria, carla).
madre(paula, pablo).
madre(paula, mateo).
madre(carla, andres).
madre(lorena, edwin).
hermanos(X,Y):-
  padre(Padre,X),
  padre(Padre,Y).
tio(X,Y):-
  hermanos(X,P),
  (padre(P,Y);
  madre(P,Y)).
antepasado(X,Y):-
  padre(X,Y);
  madre(X,Y).
antepasado(X,Y):-
  (padre(X,Z);
  madre(X,Z)),
  antepasado(Z,Y).
vertice(1,3).
vertice(3,2).
vertice(3,4).
vertice(2,4).
conectados_1(X,Y):-
  vertice(X,P),
  vertice(P,Y).
