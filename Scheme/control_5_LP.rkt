#lang scheme
(require csv-reading)
(define lista_empleados
(csv->list
    (make-csv-reader
     (open-input-file "/Users/Eduardo/Downloads/HR_comma_sep.csv")
     '(
       (separator-chars            #\,)
       (strip-leading-whitespace?   . #t)
       (strip-trailing-whitespace?  . #t)))))
;PREGUNTA 1
(define (cero a)
  (if (= a 0)
      #f
      #t))
(define (verificar_abandono lis_simple)
  (if (= (string->number (car (cdr (cdr (cdr (cdr (cdr (cdr lis_simple)))))))) 1)
      (string->number (car lis_simple))
      0))
(define listasat (filter cero (map verificar_abandono lista_empleados)))
(define (sumar lista)
  (if (empty? lista)
      0
          (+ (car lista) (sumar (cdr lista)))
          ))
(define (calcular_promedio_satisfaccion lista)
  (/ (sumar lista) (length lista)))
(calcular_promedio_satisfaccion listasat)

;PREGUNTA 2

(define (areas lista_empleados)
    (if (empty? lista_empleados)
      null
      (cons (car (cdr (cdr (cdr (cdr (cdr (cdr (cdr (cdr (car lista_empleados))))))))))
            (areas (cdr lista_empleados)))))
(define lista_areas (areas lista_empleados))