def mayuscula(cadena):
	return cadena.upper()

def funcion(f):
	return f("hola")

def imprimir():
	print("Hola mundo")
	print("Chau mundo")

def main():
	print(funcion(mayuscula))
	numero = 12
	numero = "doce"
	imprimir()

if __name__== '__main__':
	main()
