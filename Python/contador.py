import threading

class Hilo(threading.Thread):
	def __init__(self, maximo):
		threading.Thread.__init__(self)
		self.maximo = maximo

	def run(self):
		contador = 0
		while True:
			contador = contador + 1
			print("maximo {} {}".format(self.maximo, contador))
			if contador == self.maximo:
				print("Llego al maximo")
				break

def main():
	hilo = Hilo(500)
	hilo.start()
	print("Termino main")
	hilo2 = Hilo(10000)
	hilo2.start()

if __name__=='__main__':
	main()
