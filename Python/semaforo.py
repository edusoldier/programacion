import threading

class Recurso:
	def __init__(self):
		self.semaforo = threading.Semaphore(1)
	
	def consumir(self, hilo):
		self.semaforo.acquire()
		print("Hilo {}".format(hilo._id))
		print("Se finalizo Hilo {}".format(hilo._id))
		self.semaforo.release()

class Consumidor(threading.Thread):
	def __init__(self, recurso, id):
		threading.Thread.__init__(self)
		self.recurso = recurso
		self._id = id
	
	def run(self):
		self.recurso.consumir(self)

def main():
	recurso = Recurso()
		c.start()
		print ("hola")

if __name__ == '__main__':
	main()

