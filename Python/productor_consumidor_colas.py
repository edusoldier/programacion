import threading
from threading import Semaphore

class DataBuffer(threading.Thread):
	def __init__(self, cantidad_tope, max_threads_consumidor, max_threads_productor):
		threading.Thread.__init__(self)
		self.buffer_tope = cantidad_tope
		self.cola_productores = []
		self.cola_consumidores = []
		self.buffer = []
		self.semaforo_productores = Semaphore(max_threads_productor)
		self.semaforo_consumidores = Semaphore(max_threads_consumidor)
	def run(self):
		while True:
			self.semaforo_productores.acquire()
			if len(self.buffer) < self.buffer_tope and len(self.cola_productores) > 0:
				primer_productor = self.cola_productores.pop()
				primer_productor.producir()
			self.semaforo_productores.release()
			self.semaforo_consumidores.acquire()
			if len(self.buffer) > 0 and len(self.cola_consumidores) > 0:
				primer_consumidor = self.cola_consumidores.pop()
				primer_consumidor.consumir()
			self.semaforo_consumidores.release()

class Productor(threading.Thread):
	def __init__(self, id, databuffer):
		threading.Thread.__init__(self)
		self.databuffer = databuffer
		self._id = id
	def run(self):
		self.databuffer.cola_productores.append(self)
	def producir(self):
		self.databuffer.buffer.append("P{}", self._id)

class Consumidor(threading.Thread):
	def __init__(self, id, databuffer):
		threading.Thread.__init__(self)
		self.databuffer = databuffer
		self._id = id
	def run(self):
		self.databuffer.cola_consumidores.append(self)
	def consumir(self):
		self.databuffer.buffer.pop()
		print (valor_a_consumir)

def main():
	sincronizador = DataBuffer(30, 1, 1)
	sincronizador.start()
	
	for i in range(500):
		p = Productor(i+1, sincronizador)
		p.start()

	for j in range(500):
		c = Consumidor(j+1, sincronizador)
		c.start()

if __name__=='__main__':
	main()
